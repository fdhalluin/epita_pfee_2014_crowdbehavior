﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SliderUI : MonoBehaviour {

    public SliderUI sliderNeigh1;
    public SliderUI sliderNeigh2;
    [HideInInspector]
    public Slider currentSlider;
    private float oldValue;
    [HideInInspector]
    public Text attachedText;

    void Start()
    {
        currentSlider = GetComponent<Slider>();
        oldValue = currentSlider.value;
        attachedText = GetComponentInChildren<Text>();
    }


	public void AdaptOthersValue(float value)
    {
        if (this.currentSlider.interactable)
        {
            sliderNeigh1.currentSlider.interactable = false;
            sliderNeigh2.currentSlider.interactable = false;

            float valueChange = value - oldValue;

            if (sliderNeigh2.currentSlider.value - (valueChange / 2) <= 0)
            {
                float tmp = sliderNeigh2.currentSlider.value;
                sliderNeigh2.currentSlider.value = 0;
                sliderNeigh1.currentSlider.value -= (valueChange - tmp);

            }
            else if (sliderNeigh1.currentSlider.value - (valueChange / 2) <= 0)
            {
                float tmp2 = sliderNeigh1.currentSlider.value;
                sliderNeigh1.currentSlider.value = 0;
                sliderNeigh2.currentSlider.value -= (valueChange - tmp2);
            }
            else
            {
                sliderNeigh1.currentSlider.value -= valueChange / 2;
                sliderNeigh2.currentSlider.value -= valueChange / 2;
            }


            if (sliderNeigh1.currentSlider.value < 0.001)
                sliderNeigh1.currentSlider.value = 0;
            if (sliderNeigh2.currentSlider.value < 0.001)
                sliderNeigh2.currentSlider.value = 0;

            oldValue = value;
            sliderNeigh1.oldValue = sliderNeigh1.currentSlider.value;
            sliderNeigh2.oldValue = sliderNeigh2.currentSlider.value;

            attachedText.text = ((int)value).ToString();
            sliderNeigh2.attachedText.text = ((int)sliderNeigh2.currentSlider.value).ToString();
            sliderNeigh1.attachedText.text = ((int)sliderNeigh1.currentSlider.value).ToString();

            sliderNeigh1.currentSlider.interactable = true;
            sliderNeigh2.currentSlider.interactable = true;
        }
    }
}
