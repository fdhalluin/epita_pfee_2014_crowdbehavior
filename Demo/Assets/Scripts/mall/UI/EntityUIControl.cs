﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EntityUIControl : MonoBehaviour {

    ShopperGenerator generator;
    public Text entityCountText;
    public Slider entityCountSlider;

    public Text gameSpeedText;

    public Slider blueSlider;
    public Slider redSlider;
    public Slider greenSlider;

    public Text radiusText;

    public SelectEntity.Entity selectedEntity;

	// Use this for initialization
	void Start () {
        generator = GameObject.FindGameObjectWithTag("ShopperGenerator").GetComponent<ShopperGenerator>();

		// Triggers NullReferenceException for me
//        entityCountSlider.value = generator.NumberOfShoppers;
//        entityCountText.text = generator.NumberOfShoppers.ToString();
	}

    public void setEntityCount(float value)
    {
        generator.NumberOfShoppers = (int)value;
        entityCountText.text = value.ToString();
    }

    public void ChangeDistributionValues()
    {
        float[] newWeights = new float[3];

        newWeights[0] = blueSlider.value;
        newWeights[1] = redSlider.value;
        newWeights[2] = greenSlider.value;

        generator.Weights = newWeights;
    }

    public void ChangeGameSpeed(float value)
    {
        Time.timeScale = value;
        gameSpeedText.text = value.ToString();
    }

    public void ChangeRadiusMultiplier(float value)
    {
        GlobalEntities.radiusMultiplier = value;
        radiusText.text = value.ToString();
    }
}
