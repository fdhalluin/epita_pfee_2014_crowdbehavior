﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MinMaxSlider : MonoBehaviour {

    public enum Attribute
    {
        SPEED,
        STRENGTH,
        AWARENESS,
        EAGER
    }

    public Slider affectedSlider;
    public Text relatedText;
    public ShopperTemplate selectedTemplate;
    public Attribute templateAttributeToChange;

    public void ChangeMinVal(float value)
    {
        affectedSlider.minValue = value;        
    }

    public void ChangeMaxVal(float value)
    {
        affectedSlider.maxValue = value;
    }

    public void ChangeTemplateMinValue(float value)
    {
        if (Time.time != 0f)
        {
            if (templateAttributeToChange == Attribute.SPEED)
                selectedTemplate.SpeedDistribution.MinValue = value / 100;
            else if (templateAttributeToChange == Attribute.STRENGTH)
                selectedTemplate.StrengthDistribution.MinValue = value / 100;
            else if (templateAttributeToChange == Attribute.AWARENESS)
                selectedTemplate.AwarenessDistribution.MinValue = value / 100;
            else if (templateAttributeToChange == Attribute.EAGER)
                selectedTemplate.EagerDistribution.MinValue = value / 100;
        }
    }

    public void ChangeTemplateMaxValue(float value)
    {
        if (Time.time != 0f)
        {
            if (templateAttributeToChange == Attribute.SPEED)
                selectedTemplate.SpeedDistribution.MaxValue = value / 100;
            else if (templateAttributeToChange == Attribute.STRENGTH)
                selectedTemplate.StrengthDistribution.MaxValue = value / 100;
            else if (templateAttributeToChange == Attribute.AWARENESS)
                selectedTemplate.AwarenessDistribution.MaxValue = value / 100;
            else if (templateAttributeToChange == Attribute.EAGER)
                selectedTemplate.EagerDistribution.MaxValue = value / 100;
        }
    }

    public void ChangeRelatedText(float value)
    {
        relatedText.text = value.ToString();
    }
}
