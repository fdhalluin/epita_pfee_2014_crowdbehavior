﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShopperGlobal : CachedBase
{
        public static int shopperId = 0;

		public ShopperNavigation navigationEntity;
		public ShopperBehavior behaviorEntity;
		public ShopperCoroutine coroutineEntity;

        // STATES ENTITY
        public ShopperAlert alertBehaviorEntity;
        public ShopperAction actionBehaviorEntity;
        public ShopperIdle idleBehaviorEntity;

        public Animator anim;

		// This put transform and rigidbody in cache
		public override void Awake ()
		{
				base.Awake (); //does the caching.
				//Debug.Log ("Awake called!");
		}

		void Start ()
		{
			navigationEntity = gameObject.GetComponent<ShopperNavigation> () as ShopperNavigation;
			behaviorEntity = gameObject.GetComponent<ShopperBehavior> () as ShopperBehavior;
		    coroutineEntity = gameObject.GetComponent<ShopperCoroutine> () as ShopperCoroutine;

            alertBehaviorEntity = gameObject.GetComponent<ShopperAlert>() as ShopperAlert;
            actionBehaviorEntity = gameObject.GetComponent<ShopperAction>() as ShopperAction;
            idleBehaviorEntity = gameObject.GetComponent<ShopperIdle>() as ShopperIdle;

            anim = this.GetComponentInChildren<Animator>();

            shopperId++;

            name += shopperId;
		}

		// Update is called once per frame
		void Update ()
		{
            if (behaviorEntity.state == ShopperBehavior.State.IDLE)
            {
                navigationEntity.Move();
            }
		}

        public void GetKilled()
        {
            Debug.Log(this.name + " get killed!");
            Destroy(this.gameObject);
        }

        void OnDestroy()
        {
            --GlobalEntities.shopperGenerator.CurrentShopperCount;
        }

        public void SetAnimToFight()
        {
            anim.SetBool("isFighting", true);
        }

        public void SetAnimToIdle()
        {
            anim.SetBool("isFighting", false);
        }
}
