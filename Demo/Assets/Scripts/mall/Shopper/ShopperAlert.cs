﻿using UnityEngine;
using System.Collections;

public class ShopperAlert : CachedBase {

    ShopperGlobal alertCreator;
    Vector3 lastLookedPosition;
    public float timeOnStopAlert;

    public ShopperGlobal AlertCreator
    {
        get { return alertCreator; }
        set { alertCreator = value; }
    }


    // This put transform and rigidbody in cache
    public override void Awake()
    {
        base.Awake(); //does the caching.
        //Debug.Log ("Awake called!");
    }

	// Use this for initialization
	void Start () {
	
	}

    public void LookAtAlert()
    {
        ShopperAction action = GetComponent<ShopperAction>();

        if (action.fightArea)
            lastLookedPosition = action.fightArea.transform.position;
        else if (alertCreator != null)
            lastLookedPosition = new Vector3(alertCreator.transform.position.x, this.transform.position.y, alertCreator.transform.position.z);
        
        this.transform.LookAt(lastLookedPosition);
    }

}
