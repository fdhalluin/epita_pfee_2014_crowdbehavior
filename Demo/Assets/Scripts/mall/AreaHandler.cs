﻿using UnityEngine;
using System.Collections;

public class AreaHandler : MonoBehaviour {

    public GameObject prefab;
    public LayerMask mask;

	
	// Update is called once per frame
	void Update () {
        //if (Input.GetMouseButtonDown(0))
        //{
        //    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //    RaycastHit hit;
        //    if (Physics.Raycast(ray, out hit, mask))
        //    {
        //        int rand = (int)Random.Range(0, 2);
        //        createArea(hit.point, (Area.AreaType)rand, 5f, 5, null);
        //    }
        //}
	}

    public void createArea(Vector3 position, Area.AreaType type, float duration, float radius, ShopperGlobal originalCreator)
    {
        GameObject o = (GameObject)MonoBehaviour.Instantiate(prefab, position, this.transform.rotation);
        o.transform.parent = this.transform;

        Area area = o.GetComponent<Area>() as Area;
        area.Initialize(type, duration, radius, originalCreator);
    }

	public void createArea(Vector3 position, Area.AreaType type, float duration, float radius, ShopperGlobal originalCreator, ShopperGlobal otherShopper)
	{
		GameObject o = (GameObject)MonoBehaviour.Instantiate(prefab, position, this.transform.rotation);
		o.transform.parent = this.transform;
		
		Area area = o.GetComponent<Area>() as Area;
		area.Initialize(type, duration, radius, originalCreator);
	}

}
