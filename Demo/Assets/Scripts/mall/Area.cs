﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Area : MonoBehaviour {

    public enum AreaType
    {
        STEAL,
        FIGHT,
        ALERT_VIRALITY
    };

    private float radius = 10;
    public float lifeTime = 5; // in seconds
    public AreaType areaType;
    public ShopperGlobal originalCreator;

	// Fight data
	public float threshold = 100f;
    public float fightSum = 0;
	public int goodSum = 0;
	public int badSum = 0;

    // List of agent in the area
    public List<ShopperGlobal> agentInArea = new List<ShopperGlobal>();


    public void Initialize(AreaType type, float duration, float radius, ShopperGlobal originalCreator)
    {
        this.areaType = type;
        this.lifeTime = duration;
        this.radius = radius * GlobalEntities.radiusMultiplier;
        this.originalCreator = originalCreator;

        // Change area color depending on its type
        Renderer[] rendererList = this.GetComponentsInChildren<Renderer>();
        foreach (Renderer render in rendererList)
        {
            render.material = GlobalEntities.areaMaterials[(int)this.areaType];
        }

        // Set size of area
        this.transform.localScale = new Vector3((float)(this.radius), 0.1f, (float)(this.radius));

		// Safety
		if (type == AreaType.FIGHT) {
			originalCreator.actionBehaviorEntity.fightArea = this;
			originalCreator.actionBehaviorEntity.fightThreshold = threshold;
			++this.goodSum;
			originalCreator.actionBehaviorEntity.shopperToChase.actionBehaviorEntity.fightArea = this;
			originalCreator.actionBehaviorEntity.shopperToChase.actionBehaviorEntity.fightThreshold = -1 * threshold;
			++this.badSum;
		}
    }

//	void Start() {
//		Vector3 pos = this.transform.position;
//		this.transform.position = new Vector3(-1000, -1000, -1000);
//		this.transform.position = pos;
//	}

    void Update()
    {
        lifeTime -= Time.deltaTime;

		if (lifeTime <= 0) {
			if (this.areaType == AreaType.FIGHT) {
				lifeTime = 1000000000000000000000000f;
			} else {
				Destroy(this.gameObject);
			}
		} else if (this.areaType == AreaType.ALERT_VIRALITY) {
			if (lifeTime > 100000000f && originalCreator.behaviorEntity.state < ShopperBehavior.State.FIGHT_BAD) {
				Destroy(this.gameObject);
			}
		}
	}

public float Radius
    {
        get { return radius; }
        set {
            radius = value;
            this.transform.localScale = new Vector3((float)(this.radius), 0.1f, (float)(this.radius));
        }
    }

    void OnTriggerEnter(Collider other)
    {
        ShopperGlobal agent = other.GetComponent<ShopperGlobal>();
        if (agent != null)
        {
            agentInArea.Add(agent);
			if (this.areaType == AreaType.FIGHT)// && agent.behaviorEntity.state > ShopperBehavior.State.FIGHT_GOOD) {
				agent.actionBehaviorEntity.fightArea = this;
        }
    }

	void OnDestroy()
    {
        // Small hack to call onTriggerExit
        this.transform.position = new Vector3(-1000, -1000, -1000);
    }

    void OnTriggerStay(Collider other)
    {
        if (this.areaType == AreaType.FIGHT)
        {
            ShopperGlobal agent = other.GetComponent<ShopperGlobal>();
            if (agent != null)
            {
                if (agent.behaviorEntity.state == ShopperBehavior.State.ACTION)
                {
                    /*
                    * CHANGING STATE FOR PICKPOCKET
                    */
                    agent.SetAnimToFight();
                    agent.behaviorEntity.state = ShopperBehavior.State.FIGHT_BAD;
                    agent.actionBehaviorEntity.shopperToSteal = null;

                    // Will be used later for reorientation when going back to idle state
                    agent.idleBehaviorEntity.lastNodeInIdleState = agent.idleBehaviorEntity.currentNode;

                    // Change color depending on state
                    Renderer[] rendererList = agent.GetComponentsInChildren<Renderer>();
                    foreach (Renderer render in rendererList)
                        render.material = GlobalEntities.stateMaterials[(int)ShopperBehavior.State.FIGHT_BAD];

                    ++this.badSum;
                }
                else if (agent.behaviorEntity.state == ShopperBehavior.State.STEAL) 
                {
                    /*
                    * CHANGING STATE FOR PICKPOCKET
                    */
                    agent.SetAnimToFight();
                    agent.behaviorEntity.state = ShopperBehavior.State.FIGHT_GOOD;
                    agent.actionBehaviorEntity.shopperToSteal = null;

                    // Will be used later for reorientation when going back to idle state
                    agent.idleBehaviorEntity.lastNodeInIdleState = agent.idleBehaviorEntity.currentNode;

                    // Change color depending on state
                    Renderer[] rendererList = agent.GetComponentsInChildren<Renderer>();
                    foreach (Renderer render in rendererList)
                        render.material = GlobalEntities.stateMaterials[(int)ShopperBehavior.State.FIGHT_GOOD];

                    ++this.goodSum;
                }
                else if (agent.behaviorEntity.state == ShopperBehavior.State.IDLE)
                {
                    agent.behaviorEntity.state = ShopperBehavior.State.ALERT;
                    agent.alertBehaviorEntity.timeOnStopAlert = this.lifeTime;
                    //Debug.Log("Kikoo" + agent.name);
                    // Change color depending on state
                    Renderer[] rendererList = agent.GetComponentsInChildren<Renderer>();
                    foreach (Renderer render in rendererList)
                        render.material = GlobalEntities.stateMaterials[(int)ShopperBehavior.State.ALERT];
                }
            }
        }
    }

}
