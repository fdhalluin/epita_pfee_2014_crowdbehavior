﻿using UnityEngine;
using System.Collections;
using RVO;
using System.Collections.Generic;

public class MainRVO : MonoBehaviour {

    public List<Vector3> pList1 = new List<Vector3>();
	public List<Vector3> pList2 = new List<Vector3>();

	// Use this for initialization
	void Start ()
	{
        GameObject[] shopList = GameObject.FindGameObjectsWithTag("Shop");
        foreach (GameObject shop in shopList)
        {
            foreach (Transform child in shop.transform)
            {
                IList<RVO.Vector2> obstacle = new List<RVO.Vector2>();
                Mesh mesh = child.GetComponent<MeshFilter>().mesh;
                            
                

                Vector3 p1 = child.TransformPoint(mesh.vertices[0]);
                Vector3 p2 = child.TransformPoint(mesh.vertices[1]);
                Vector3 p3 = child.TransformPoint(mesh.vertices[6]);
                Vector3 p4 = child.TransformPoint(mesh.vertices[7]);

				Vector3 topleft = findTopLeft (new List<Vector3>(new Vector3[] {p1,p2,p3,p4}));
				Vector3 botleft = findBotLeft (new List<Vector3>(new Vector3[] {p1,p2,p3,p4}));;
				Vector3 topright = findTopRight (new List<Vector3>(new Vector3[] {p1,p2,p3,p4}));;
				Vector3 botright = findBotRight (new List<Vector3>(new Vector3[] {p1,p2,p3,p4}));;

                Vector3 tmp1 = Vector3.Max(p3, p2);

				obstacle.Add(new RVO.Vector2(topleft.x, topleft.z));
				obstacle.Add(new RVO.Vector2(botleft.x, botleft.z));
				obstacle.Add(new RVO.Vector2(botright.x, botright.z));
				obstacle.Add(new RVO.Vector2(topright.x, topright.z));


                Simulator.Instance.addObstacle(obstacle);
			}
        }

        Simulator.Instance.processObstacles();
    }

	internal Vector3 findTopLeft(List<Vector3> l)
	{
		Vector3 res = l[0];
		foreach (var v in l)
			if (res.x >= v.x && res.z <= v.z)
				res = v;
		return res;
	}

	internal Vector3 findBotLeft(List<Vector3> l)
	{
		Vector3 res = l[0];
		foreach (var v in l)
			if (res.x >= v.x && res.z >= v.z)
				res = v;
		return res;
	}

	internal Vector3 findTopRight(List<Vector3> l)
	{
		Vector3 res = l[0];
		foreach (var v in l)
			if (res.x <= v.x && res.z <= v.z)
				res = v;
		return res;
	}

	internal Vector3 findBotRight(List<Vector3> l)
	{
		Vector3 res = l[0];
		foreach (var v in l)
			if (res.x <= v.x && res.z >= v.z)
				res = v;
		return res;
	}

    void Update()
    {
		Simulator.Instance.setTimeStep(Time.deltaTime);
		Simulator.Instance.doStep();
    }

	
}
