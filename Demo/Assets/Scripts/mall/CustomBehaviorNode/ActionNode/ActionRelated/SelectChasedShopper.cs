﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class SelectChasedShopper : ActionNode {

    public GameObjectVar gameObject;

    // Called when the node will be ticked
    public override void OnTick () {
        GameObject unityGameObj = (GameObject)gameObject;
        Area correspodingNode = unityGameObj.GetComponent<Area>();

        if (correspodingNode != null)
        {
            ShopperAction actionBehaviorShopper = self.GetComponent<ShopperAction>();

            actionBehaviorShopper.shopperToChase = correspodingNode.originalCreator;

            status = Status.Success;
        }
        else
            status = Status.Failure;
    }
}
