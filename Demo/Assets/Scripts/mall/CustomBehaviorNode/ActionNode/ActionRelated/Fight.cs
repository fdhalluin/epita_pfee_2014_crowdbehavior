﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;

public class Fight : ActionNode {

    public FloatVar fightRadius;

	public override void OnTick() {

        ShopperGlobal shopperGlobal = self.GetComponent<ShopperGlobal>();
        ShopperAction action = shopperGlobal.actionBehaviorEntity;
		ShopperBehavior behavior = shopperGlobal.behaviorEntity;
        
        // May cause trouble when not enough place to fight
        float distanceToAreaCenter = Vector3.Distance(shopperGlobal.transform.position, action.fightArea.transform.position);

        if (distanceToAreaCenter >= fightRadius)
        {
            if (distanceToAreaCenter >= action.fightArea.Radius / 4)
                shopperGlobal.navigationEntity.disableRVO = true;
            else
                shopperGlobal.navigationEntity.disableRVO = false;

            // GOING TO CENTER
            shopperGlobal.navigationEntity.TargetPosition = action.fightArea.transform.position;
            shopperGlobal.navigationEntity.Move();
            
            status = Status.Running;
            return;
        }

		if (action.fightArea) {
            // Look at center (hotfix to be improved)
            shopperGlobal.alertBehaviorEntity.LookAtAlert();
			if (behavior.state == ShopperBehavior.State.FIGHT_GOOD) {
				action.fightArea.fightSum -= behavior.strength * Time.deltaTime;
			} else {
				action.fightArea.fightSum += behavior.strength * Time.deltaTime;
			}
			status = Status.Success;
		} else {
			status = Status.Failure;
		}
	}
}
