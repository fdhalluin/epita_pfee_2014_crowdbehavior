﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class IncreaseSpeed : ActionNode
{
    public IntVar intToAdd;

    // Called when the node will be ticked
    public override void OnTick()
    {
        ShopperGlobal shopperEntity = self.GetComponent<ShopperGlobal>() as ShopperGlobal;

        shopperEntity.navigationEntity.speed += intToAdd;
        status = Status.Success;
    }
}
