﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class CreateVirality : ActionNode
{
    public GameObjectVar areaGameObj;
    // Called when the node will be ticked
    public override void OnTick()
    {
        Area area = ((GameObject)areaGameObj).GetComponent<Area>();

        if (area && area.Radius > 2)
        {
            GlobalEntities.areaHandler.createArea(self.transform.position, Area.AreaType.ALERT_VIRALITY, area.lifeTime, area.Radius / 2, area.originalCreator);
        }

        status = Status.Success;

    }
}