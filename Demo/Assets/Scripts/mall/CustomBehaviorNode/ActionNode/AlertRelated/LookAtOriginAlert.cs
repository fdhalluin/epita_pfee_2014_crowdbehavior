﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class LookAtOriginAlert : ActionNode
{

    // Called when the node will be ticked
    public override void OnTick()
    {
        ShopperGlobal shopperGlobal = self.GetComponent<ShopperGlobal>();

        Area fightArea = shopperGlobal.actionBehaviorEntity.fightArea;

        if (fightArea != null)
        {
            float distanceToCenterArea = Vector3.Distance(shopperGlobal.transform.position, fightArea.transform.position);

            if (distanceToCenterArea < fightArea.Radius / 2 - 1) {

                Vector3 tmp = fightArea.transform.position;
                tmp.x -= fightArea.transform.position.x - tmp.x;
                tmp.z -= fightArea.transform.position.z - tmp.z;
                shopperGlobal.navigationEntity.disableRVO = true;
                shopperGlobal.navigationEntity.TargetPosition = tmp * -1;
                shopperGlobal.navigationEntity.Move();

                status = Status.Running;
                return;
            }
        }

        shopperGlobal.navigationEntity.disableRVO = false;
        shopperGlobal.alertBehaviorEntity.LookAtAlert();

        status = Status.Success;
    }
}
