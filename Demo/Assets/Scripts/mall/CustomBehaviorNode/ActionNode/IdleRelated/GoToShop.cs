﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
using System.Collections.Generic;
public class GoToShop : ActionNode
{

    // Called when the node will be ticked
    public override void OnTick()
    {
        // Get shopper
        ShopperGlobal shopperEntity = self.GetComponent<ShopperGlobal>() as ShopperGlobal;

        // If arrived at shop node
        if (shopperEntity.idleBehaviorEntity.currentNode == shopperEntity.idleBehaviorEntity.currentTargetShopNode)
        {
            shopperEntity.navigationEntity.disableRVO = true;
            shopperEntity.navigationEntity.TargetPosition = shopperEntity.idleBehaviorEntity.shopPositionList[0];
        }
            // If arrived near the target position
        else if (shopperEntity.idleBehaviorEntity.targetNode != null && shopperEntity.idleBehaviorEntity.currentTargetShopNode != shopperEntity.idleBehaviorEntity.targetNode)
        {
            if (Vector3.Distance(shopperEntity.transform.position, shopperEntity.idleBehaviorEntity.targetNode.cachedTransform.position) < shopperEntity.idleBehaviorEntity.targetNode.Radius)
            {
                if (shopperEntity.idleBehaviorEntity.wantToExit == true)
                {
                    // If not arrived at exit
                    if (shopperEntity.idleBehaviorEntity.currentNode.nextNodeToExit)
                        shopperEntity.idleBehaviorEntity.targetNode = shopperEntity.idleBehaviorEntity.currentNode.nextNodeToExit;

                }
                // Not arrived to current goal
                else if (shopperEntity.idleBehaviorEntity.nodeListToCurrentTargetShop.Count > 1)
                {
                    shopperEntity.idleBehaviorEntity.nodeListToCurrentTargetShop.RemoveAt(0);
                    shopperEntity.idleBehaviorEntity.targetNode = shopperEntity.idleBehaviorEntity.nodeListToCurrentTargetShop[0];
                }

                shopperEntity.navigationEntity.TargetPosition = shopperEntity.idleBehaviorEntity.targetNode.transform.position;
            }
        }

        status = Status.Success;
    }
}
