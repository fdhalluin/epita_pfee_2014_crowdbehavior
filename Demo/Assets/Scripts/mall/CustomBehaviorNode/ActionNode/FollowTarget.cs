﻿
using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class FollowTarget : ActionNode
{

    // Called when the node will be ticked
    public override void OnTick()
    {
        ShopperGlobal shopperEntity = self.GetComponent<ShopperGlobal>() as ShopperGlobal;

        if (shopperEntity.actionBehaviorEntity.shopperToSteal)
        {
            shopperEntity.navigationEntity.TargetPosition = shopperEntity.actionBehaviorEntity.shopperToSteal.transform.position;
            //shopperEntity.navigationEntity.speed *= 1.5f;
            float distanceToTargetShopper = Vector3.Distance(shopperEntity.transform.position, shopperEntity.actionBehaviorEntity.shopperToSteal.transform.position);

            //Debug.Log("Distance: " + distanceToTargetShopper);

            // Disable RVO when getting close in order to avoid RVO preventing collision
            if (distanceToTargetShopper < 1.5)
                shopperEntity.navigationEntity.disableRVO = true;

            if (distanceToTargetShopper > 0.6)
            {
                Vector3 pos = shopperEntity.transform.position;
                shopperEntity.navigationEntity.Move();

                status = Status.Running;
                return;
            }

            //shopperEntity.actionBehaviorEntity.shopperToSteal = null;
            float duration = Random.Range(3, 7);
            GlobalEntities.areaHandler.createArea(self.transform.position, Area.AreaType.STEAL, duration, 10, shopperEntity);
        }
        shopperEntity.navigationEntity.disableRVO = false;
            
        status = Status.Success;
    }
}
