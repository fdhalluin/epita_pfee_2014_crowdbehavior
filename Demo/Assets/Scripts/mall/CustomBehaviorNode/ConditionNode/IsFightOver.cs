﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;

public class IsFightOver : ConditionNode
{
	
	public override void OnTick() {
		status = Status.Failure;

        ShopperGlobal globalShopper = self.GetComponent<ShopperGlobal>();
        ShopperAction action = globalShopper.actionBehaviorEntity;
        ShopperBehavior behavior = globalShopper.behaviorEntity;
		
		if (action.fightArea) {
			if (((action.fightArea.goodSum <= 0 || action.fightArea.badSum <= 0) && Mathf.Abs(action.fightArea.fightSum) >= action.fightArea.threshold) ||
			    ((action.fightArea.goodSum == 1 || action.fightArea.badSum == 1) && Mathf.Abs(action.fightArea.fightSum) > 2f * action.fightArea.threshold + 1f)) {
				Area fightArea = action.fightArea;
				foreach (ShopperGlobal global in action.fightArea.agentInArea) {
					if (global) {
						global.actionBehaviorEntity.fightArea = null;
						global.actionBehaviorEntity.fightThreshold = 0f;
						global.alertBehaviorEntity.timeOnStopAlert = 0f;
					}
				}
				GlobalEntities.Destroy(fightArea);
				status = Status.Success;
                globalShopper.SetAnimToIdle();
			}
		} else {
            globalShopper.SetAnimToIdle();
			status = Status.Success;
		}
	}
}
