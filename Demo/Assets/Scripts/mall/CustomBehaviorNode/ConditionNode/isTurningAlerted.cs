using UnityEngine;
using System.Collections;
using BehaviourMachine;

public class isTurningAlerted : ConditionNode
{
		public FloatVar threshold = 0.5f;
        public GameObjectVar gameObject;

		// Called when the node will be ticked
		public override void OnTick ()
		{
            GameObject unityGameObj = (GameObject)gameObject;
            Area area = unityGameObj.GetComponent<Area>();

            ShopperBehavior shopperBehavior = self.GetComponent<ShopperBehavior>();

            if (area.areaType == Area.AreaType.FIGHT)
            {
                status = Status.Success;
                return;
            }
                

			RandomDistribution distr = self.GetComponent<ShopperTemplate>().AwarenessDistribution;
            float awareness = (shopperBehavior.awareness - distr.MinValue) / (distr.MaxValue - distr.MinValue);

			if (Random.value < awareness && Random.value > threshold) {
					status = Status.Success;
			} else {
					status = Status.Failure;
			}
		}
}

