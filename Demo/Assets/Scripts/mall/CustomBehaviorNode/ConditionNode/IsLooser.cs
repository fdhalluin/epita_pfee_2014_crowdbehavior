﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;

public class IsLooser : ConditionNode
{

	public override void OnTick() {
		status = Status.Failure;

        ShopperGlobal global = self.GetComponent<ShopperGlobal>();
        ShopperAction action = global.actionBehaviorEntity;
		ShopperBehavior behavior = global.behaviorEntity;
		
		if (action.fightArea) {
			if (behavior.state == ShopperBehavior.State.FIGHT_GOOD && action.fightArea.fightSum >= action.fightThreshold) {
				--action.fightArea.goodSum;
				status = Status.Success;
                global.SetAnimToIdle();
			} else if (behavior.state == ShopperBehavior.State.FIGHT_BAD && action.fightArea.fightSum <= action.fightThreshold) {
				--action.fightArea.badSum;
				status = Status.Success;
                global.SetAnimToIdle();
			}
		}
	}
}
