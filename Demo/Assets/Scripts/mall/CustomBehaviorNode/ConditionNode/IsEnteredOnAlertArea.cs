﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class IsEnteredOnArea : ConditionNode {

    public GameObjectVar gameObject;

    // Called when the node will be ticked
    public override void OnTick () {
        GameObject unityGameObj = (GameObject)gameObject;
        Area correspodingNode = unityGameObj.GetComponent<Area>();

        if (correspodingNode != null)
        {
            ShopperAlert alertBehaviorShopper = self.GetComponent<ShopperAlert>();
            ShopperAction actionBehaviorShopper = self.GetComponent<ShopperAction>();

            alertBehaviorShopper.AlertCreator = correspodingNode.originalCreator;
            alertBehaviorShopper.timeOnStopAlert = Time.time + correspodingNode.lifeTime;

            
            status = Status.Success;
        }
        else
            status = Status.Failure;
    }
}
