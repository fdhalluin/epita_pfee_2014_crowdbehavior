﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class IsStateEqual : ConditionNode
{

    //public GameObject gameObject;
    public ShopperBehavior.State state;

    // Called when the node will be ticked
    public override void OnTick()
    {
        status = Status.Failure;
        ShopperGlobal shopperEntity = self.GetComponent<ShopperGlobal> () as ShopperGlobal;

		if (shopperEntity != null) {
            if (shopperEntity.behaviorEntity.state == this.state)
                status = Status.Success;
        }

        //ShopperBehavior.State currentState = (ShopperBehavior.State)blackboard.GetIntVar("State").Value;


    }
}
