﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
using System.Collections.Generic;
public class IsArrivedAtShopNode : ConditionNode
{

    // Called when the node will be ticked
    public override void OnTick()
    {
        status = Status.Failure;

        ShopperIdle idleEntity = self.GetComponent<ShopperIdle>() as ShopperIdle;

        if (idleEntity.currentNode == idleEntity.currentTargetShopNode)
        {
            status = Status.Success;
        }
        
    }
}
