﻿using UnityEngine;
using System.Collections;

public class Shop : MonoBehaviour
{

		public NavNode entryNode;

		public Vector2 posBound1;
		public Vector2 posBound2;
		public Vector2 posBound3;
		public Vector2 posBound4;
		public int Size;

		// Use this for initialization
		void Start ()
		{
		}
	
		// Update is called once per frame
		void Update ()
		{
				Mesh mesh = this.GetComponent<MeshFilter> ().mesh;

				var transf = this.transform;

				Vector3 b1 = transf.TransformPoint (mesh.vertices [0]);
				Vector3 b2 = transf.TransformPoint (mesh.vertices [1]);
				Vector3 b3 = transf.TransformPoint (mesh.vertices [4]);
				Vector3 b4 = transf.TransformPoint (mesh.vertices [5]);

				Size = mesh.vertices.Length;
				Renderer render = this.GetComponent<Renderer> () as Renderer;
				posBound1 = new Vector2 (b1.x, b1.z);
				posBound2 = new Vector2 (b2.x, b2.z);
				posBound3 = new Vector2 (b3.x, b3.z);
				posBound4 = new Vector2 (b4.x, b4.z);
		}

		void OnTriggerEnter (Collider other)
		{
				NavNode correspodingNode = other.GetComponent<NavNode> ();
				if (correspodingNode != null && entryNode == null) {
						entryNode = correspodingNode;
				} else {
						ShopperGlobal agent = other.GetComponent<ShopperGlobal> ();
						if (agent != null) {
								if (agent.idleBehaviorEntity.currentTargetShopNode == entryNode) {
										agent.idleBehaviorEntity.ChangeGoalNode ();
								}
						}
				}
		}

}
