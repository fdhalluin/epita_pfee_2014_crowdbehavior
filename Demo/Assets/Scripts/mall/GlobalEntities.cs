﻿using UnityEngine;
using System.Collections;
using System;

public class GlobalEntities : MonoBehaviour
{

		public static GameObject[]  nodeList;
		public static GameObject[]  spawnNodeList;
		public static GameObject[]  shopList;
		public static GameObject[]  doorShopList;
		public static Material[]    stateMaterials;
		public static Material[]    areaMaterials;
		public static AreaHandler areaHandler;
		
		public static ShopperGenerator shopperGenerator;

        public static float radiusMultiplier = 1f;

		// Use this for initialization
		void Start ()
		{
				nodeList = GameObject.FindGameObjectsWithTag ("NavNode");
				spawnNodeList = GameObject.FindGameObjectsWithTag ("ExitNode");
				shopList = GameObject.FindGameObjectsWithTag ("Shop");
				doorShopList = GameObject.FindGameObjectsWithTag ("ShopDoor");
				shopperGenerator = GameObject.FindGameObjectWithTag ("ShopperGenerator").GetComponent (typeof(ShopperGenerator)) as ShopperGenerator;
				areaHandler = GameObject.FindGameObjectWithTag ("AreaHandler").GetComponent (typeof(AreaHandler)) as AreaHandler;
				
				stateMaterials = new Material[Enum.GetValues (typeof(State)).GetLength (0)];
				foreach (int i in Enum.GetValues(typeof(State))) {
						stateMaterials [i] = Resources.Load<Material> (String.Concat ("Materials/States/", Enum.GetName (typeof(State), i)));
				}

				areaMaterials = new Material[Enum.GetValues (typeof(Area.AreaType)).GetLength (0)];
				foreach (int i in Enum.GetValues(typeof(Area.AreaType))) {
						areaMaterials [i] = Resources.Load<Material> (String.Concat ("Materials/Area/", Enum.GetName (typeof(Area.AreaType), i)));
						Debug.Log (areaMaterials [i]);
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public enum State
		{
				IDLE = 0,
				ALERT,
				STEAL,
				ACTION,
				DELEGATION,
				PANIC,
                FIGHT_GOOD,
                FIGHT_BAD
		}
}
