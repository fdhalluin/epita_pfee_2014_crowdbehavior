using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AStar
{

	private static Dictionary<NavNode, float> gScore = new Dictionary<NavNode, float>();
	private static Dictionary<NavNode, float> fScore = new Dictionary<NavNode, float>();
	private static Dictionary<NavNode, NavNode> parents = new Dictionary<NavNode, NavNode>();

	
	public static List<NavNode> GetPath (NavNode start, NavNode goal)
	{
		parents.Clear ();
		NavNode current;
        List<NavNode> openSet = new List<NavNode>();
        List<NavNode> closedSet = new List<NavNode>();

		
		gScore [start] = 0;
		fScore [start] = gScore [start] + Heuristic (start, goal);
        openSet.Add(start);

		while (openSet.Count != 0) {
			int curIdx = findBestNode(openSet);
			current = openSet[curIdx];

			if (current == goal)
				return ReconstructPath (goal);

			openSet.RemoveAt(curIdx);
            closedSet.Add(current);
			foreach (NavNode n in current.nodeList) {
                if (closedSet.Contains(n))
					continue;
				float tmp = gScore[current] + Heuristic(current, n);

                if (!openSet.Contains(n) || tmp < gScore[n])
                {
					parents[n] = current;
					gScore[n] = tmp;
					fScore[n] = tmp + Heuristic(n, goal);
                    if (!openSet.Contains(n))
                        openSet.Add(n);
				}
			}
		}

		return null;
	}

	private static List<NavNode> ReconstructPath (NavNode node)
	{
		List<NavNode> path = new List<NavNode> ();
		NavNode parent;

		while (true) {
			path.Insert(0, node);
			if (parents.TryGetValue(node, out parent))
				node = parent;
			else
				return path;
		}
	}

	private static float Heuristic(NavNode a, NavNode b)
	{
		return Vector3.Distance (a.transform.position, b.transform.position);
	}

    private static int findBestNode(List<NavNode> navList)
    {
        int index = 0;
        float score = -1f;
        int i = 0;

        foreach (NavNode node in navList)
        {
            if (score == -1 || score > fScore[node])
            {
                index = i;
                score = fScore[node];
            }
            i++;
        }

        return index;
    }

}
